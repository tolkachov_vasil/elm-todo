port module State exposing (..)

import List exposing (..)
import Shared.Utils exposing (..)
import Todo.State
import Tag.State
import Types exposing (..)


port setStorage : Model -> Cmd msg


inintialModel : Model
inintialModel =
  { url = "/"
  , todos = Todo.State.init
  , tags = Tag.State.init
  , newText = ""
  , selectedTodoId = -1
  , showTodoMenu = False
  }


init : Maybe Model -> ( Model, Cmd Msg )
init savedModel =
  Maybe.withDefault inintialModel savedModel ! []


withSetStorage : ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
withSetStorage ( model, cmds ) =
  ( model, Cmd.batch [ setStorage model, cmds ] )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    NoOp ->
      model ! []

    Link url ->
      { model | url = url } ! []

    NewTodoTodoClearText ->
      { model | newText = "" } ! []

    TodoSelect todo ->
      { model | selectedTodoId = todo.id } ! []

    NewTodoKeyUp a ->
      case a of
        13 ->
          addNewTodo model

        27 ->
          { model | newText = "" } ! []

        _ ->
          model ! []

    TodoDelete todo ->
      let
        allChilds =
          (allChildIds model.todos todo.childs) ++ [ todo.id ]

        notInDeletedId id =
          not (id `member` allChilds)

        notChilds t =
          not (t.id `member` allChilds)

        cleanDeletedIds t =
          { t | childs = filter notInDeletedId t.childs }

        updatedTodos =
          map cleanDeletedIds <| filter notChilds model.todos
      in
        { model
          | todos = updatedTodos
          , selectedTodoId = -1
          , showTodoMenu = False
        }
          ! []

    NewTodoTextChange text ->
      { model | newText = text } ! []

    TodoAddNew ->
      addNewTodo model

    TodoTextChange todo text ->
      let
        update t =
          if t.id == todo.id then
            { t | text = text }
          else
            t

        updatedTodos =
          map update model.todos
      in
        { model | todos = updatedTodos } ! []

    TodoCheckedChange todo ->
      let
        update t =
          if t.id == todo.id then
            { t | checked = not t.checked }
          else
            t

        updatedTodos =
          map update model.todos
      in
        { model | todos = updatedTodos } ! []

    TodoAddChild todo ->
      let
        id =
          newIdFrom model.todos

        text =
          toString (length todo.childs + 1) ++ ") "

        addChildId t =
          if t.id == todo.id then
            { t | childs = t.childs ++ [ id ], checked = False }
          else
            t

        updatedTodos =
          map addChildId model.todos ++ [ Todo.State.newTodo text id todo.folded ]
      in
        { model | todos = updatedTodos, showTodoMenu = False } ! []

    TodoFoldedChange todo ->
      let
        folded =
          todo.folded

        allChilds =
          allChildIds model.todos todo.childs

        foldChilds t =
          if t.id == todo.id then
            { t | folded = not folded }
          else if t.id `member` allChilds then
            { t | hidden = not folded, folded = not folded }
          else
            t

        updatedTodos =
          map foldChilds model.todos
      in
        { model | todos = updatedTodos } ! []

    TodoShowMenu show ->
      { model | showTodoMenu = show } ! []


addNewTodo : Model -> ( Model, Cmd Msg )
addNewTodo model =
  let
    id =
      newIdFrom model.todos

    text =
      model.newText

    updatedTodos =
      model.todos ++ [ Todo.State.newTodo text id False ]
  in
    if text /= "" then
      { model | todos = updatedTodos, showTodoMenu = False, newText = "" } ! []
    else
      model ! []
