module Shared.Utils exposing (..)

import Types exposing (..)
import List exposing (..)
import Debug
import Json.Decode as Json
import Html exposing (..)
import Html.Events exposing (..)


log : String -> a -> a
log =
  Debug.log


allChildIds : List Todo -> List Int -> List Int
allChildIds todos ids =
  case todos of
    [] ->
      ids

    todo :: rest ->
      allChildIds rest <|
        if member todo.id ids then
          todo.childs ++ ids
        else
          ids


allInnerIds : List Todo -> List Int -> List Int
allInnerIds todos ids =
  case todos of
    [] ->
      ids

    todo :: rest ->
      allInnerIds rest <| todo.childs ++ ids


(?) : Bool -> a -> a -> a
(?) cond y n =
  if cond then
    y
  else
    n


(?++) : Bool -> String -> String
(?++) cond str =
  if cond then
    " " ++ str
  else
    ""


onKeyUp : (Int -> msg) -> Attribute msg
onKeyUp tagger =
  on "keyup" (Json.map tagger keyCode)


newIdFrom : List { a | id : number } -> Int
newIdFrom list =
  Maybe.withDefault 0 (maximum <| map (.id) list) + 1


tagWithId : Int -> List Tag -> Tag
tagWithId id tags =
  let
    tag =
      head <| filter (\t -> t.id == id) <| tags
  in
    Maybe.withDefault { id = 0, text = "", color = "transparent" } tag


null =
  div [] []
