module Shared.Menu exposing (..)

import List exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Types exposing (..)
import Shared.Button exposing (..)


menu : List ( String, String, Msg ) -> Html Msg
menu items =
  div []
    [ div [ class "todo-menu__overlay", onClick <| TodoShowMenu False ] []
    , ul [ class "todo-menu" ] <|
        map
          (\( icon, txt, msg ) ->
            li [ class "todo-menu__item", onClick msg ]
              [ imgButton icon msg
              , text txt
              ]
          )
          items
    ]
