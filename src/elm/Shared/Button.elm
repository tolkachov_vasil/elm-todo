module Shared.Button exposing (..)

import Types exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


imgButton : String -> Msg -> Html Msg
imgButton classSuffix msg =
  div [ class "todo-cmp__button" ]
    [ div
        [ class <| "todo-cmp__" ++ classSuffix ++ "-button"
        , onClick msg
        ]
        []
    ]


switch : Bool -> String -> String -> Msg -> Html Msg
switch cond classPrefix classSuffix msg =
  div [ class "todo-cmp__button" ]
    [ div
        [ class <|
            classPrefix
              ++ (if cond then
                    "--" ++ classSuffix
                  else
                    ""
                 )
        , onClick msg
        ]
        []
    ]


link txt url =
  span [ class "todo-cmp__link", onClick <| Link url ] [ text txt ]
