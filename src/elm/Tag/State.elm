module Tag.State exposing (..)

import Types exposing (..)


init : List Tag
init =
  [ { id = 1, text = "work", color = "#adb" }
  , { id = 2, text = "home", color = "#caf" }
  , { id = 3, text = "hobby", color = "#4f1" }
  ]


newTag : String -> Int -> String -> Tag
newTag text id color =
  { id = id, text = text, color = color }
