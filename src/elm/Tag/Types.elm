module Tag.Types exposing (..)


type alias Tag =
  { id : Int
  , color : String
  , text : String
  }
