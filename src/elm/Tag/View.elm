module Tag.View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import List exposing (..)
import Types exposing (..)
import Shared.Utils exposing (..)


view : Tag -> Html Msg
view tag =
  div
    [ class "todo-tags__tag"
    , style [ ( "color", tag.color ) ]
    ]
    [ text tag.text ]


viewTags : Todo -> Model -> Html Msg
viewTags todo model =
  let
    tag id =
      view <| tagWithId id model.tags
  in
    div [ class "todo-tags" ] <| map tag todo.tags


viewAllTags : Model -> Html Msg
viewAllTags model =
  let
    tag tag =
      li [] [ view tag ]
  in
    ul [ class "todo-tags" ] <| map tag model.tags
