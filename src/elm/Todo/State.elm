module Todo.State exposing (..)

import Types exposing (..)


init : List Todo
init =
  [ { id = 1, tags = [], childs = [], checked = False, text = "1", deleted = False, hidden = False, folded = False }
  , { id = 2, tags = [ 1 ], childs = [ 3, 4 ], checked = False, text = "2", deleted = False, hidden = False, folded = False }
  , { id = 3, tags = [ 1, 2, 3 ], childs = [], checked = False, text = "3 in 2", deleted = False, hidden = False, folded = False }
  , { id = 4, tags = [ 2 ], childs = [], checked = False, text = "4 in 2", deleted = False, hidden = False, folded = False }
  ]


newTodo : String -> Int -> Bool -> Todo
newTodo text id hidden =
  { id = id, tags = [], childs = [], checked = False, text = text, deleted = False, hidden = hidden, folded = False }
