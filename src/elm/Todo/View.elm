module Todo.View exposing (..)

import Html exposing (Html, div, input, ul, li)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import List exposing (..)
import Html.Lazy exposing (lazy, lazy2)
import Html.Keyed as Keyed
import Types exposing (..)
import Tag.View as Tag
import Shared.Utils exposing (..)
import Shared.Button exposing (..)
import Shared.Menu exposing (..)


view : Todo -> Model -> Html Msg
view todo model =
  let
    todoIsParent =
      not <| isEmpty todo.childs

    selected =
      todo.id == model.selectedTodoId
  in
    div
      [ class <| "todo-cmp" ++ todo.hidden ?++ "todo-cmp--hidden" ++ todo.folded ?++ "todo-cmp--folded"
      , onClick <| TodoSelect todo
      , onMouseOver <| TodoSelect todo
      ]
      [ lazy2 Tag.viewTags todo model
      , lazy (todoIsParent ? foldButton <| checkButton) todo
      , lazy textInput todo
      , if selected then
          lazy2 imgButton "menu" <| TodoShowMenu True
        else
          null
      , if selected && model.showTodoMenu then
          lazy todoMenu todo
        else
          null
      ]


todoMenu : Todo -> Html Msg
todoMenu todo =
  menu
    [ ( "del", "Delete Task", TodoDelete todo )
    , ( "add", "Add Sub-task", TodoAddChild todo )
    , ( "del", "Close", TodoShowMenu False )
    ]


textInput : Todo -> Html Msg
textInput todo =
  input
    [ class <| "todo-cmp__text" ++ todo.checked ?++ "todo-cmp__text--checked"
    , type' "text"
    , value <| log "render" todo.text
    , onInput <| TodoTextChange todo
    ]
    []


foldButton : Todo -> Html Msg
foldButton todo =
  switch todo.folded "todo-cmp__fold-button" "folded" <| TodoFoldedChange todo


checkButton : Todo -> Html Msg
checkButton todo =
  switch todo.checked "todo-cmp__check-button" "checked" <| TodoCheckedChange todo


viewTodos : Model -> Html Msg
viewTodos model =
  let
    innerIds =
      allInnerIds model.todos []

    li_Todo todo =
      ( toString todo.id
      , li []
          [ lazy2 view todo model
          , lazy2 viewChilds todo model
          ]
      )

    rootOnly todo =
      not <| todo.id `member` innerIds
  in
    Keyed.ul [ class "app-todos" ] <| map li_Todo <| filter rootOnly <| reverse model.todos


viewChilds : Todo -> Model -> Html Msg
viewChilds parentTodo model =
  let
    myInner todo =
      todo.id `member` parentTodo.childs

    li_Todo todo =
      ( toString todo.id
      , li []
          [ lazy2 view todo model
          , lazy2 viewChilds todo model
          ]
      )
  in
    Keyed.ul [ class "app-todos" ] <| map li_Todo <| filter myInner model.todos
