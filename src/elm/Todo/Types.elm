module Todo.Types exposing (..)

import Tag.Types exposing (..)


type alias Todo =
  { id : Int
  , childs : List Int
  , tags : List Int
  , text : String
  , deleted : Bool
  , checked : Bool
  , hidden : Bool
  , folded : Bool
  }
