module Main exposing (..)

import Html.App as App
import Types
import State
import View


-- import TimeTravel.Html.App as TimeTravel


main : Program (Maybe Types.Model)
main =
  -- TimeTravel.programWithFlags
  App.programWithFlags
    { init = State.init
    , view = View.view
    , update = (\msg model -> State.withSetStorage (State.update msg model))
    , subscriptions = \_ -> Sub.none
    }
