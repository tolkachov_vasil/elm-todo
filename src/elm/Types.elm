module Types exposing (..)

import Todo.Types
import Tag.Types


type alias Todo =
  Todo.Types.Todo


type alias Tag =
  Tag.Types.Tag


type alias Model =
  { url : String
  , todos : List Todo
  , tags : List Tag
  , newText : String
  , selectedTodoId : Int
  , showTodoMenu : Bool
  }


type Msg
  = NoOp
  | TodoDelete Todo
  | TodoSelect Todo
  | TodoFoldedChange Todo
  | TodoAddNew
  | TodoAddChild Todo
  | TodoTextChange Todo String
  | TodoShowMenu Bool
  | TodoCheckedChange Todo
  | NewTodoTextChange String
  | NewTodoKeyUp Int
  | NewTodoTodoClearText
  | Link String
