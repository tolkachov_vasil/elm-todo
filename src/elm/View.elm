module View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (lazy, lazy2)
import Todo.View as Todo
import Tag.View as Tag
import Types exposing (..)
import Shared.Utils exposing (..)
import Shared.Button exposing (..)


view : Model -> Html Msg
view model =
  div []
    [ link "Todos" "todos"
    , link "Tags" "tags"
    , case model.url of
        "tags" ->
          tagsPage model

        _ ->
          todosPage model
    ]


todosPage : Model -> Html Msg
todosPage model =
  div [ class "todo-app" ]
    [ lazy textInput model
    , lazy Todo.viewTodos model
    ]


tagsPage : Model -> Html Msg
tagsPage model =
  div [ class "tags-list" ]
    [ Tag.viewAllTags model
    ]


textInput : Model -> Html Msg
textInput model =
  div [ class "todo-add-new" ]
    [ input
        [ class "todo-cmp__text"
        , placeholder "Enter New Task"
        , autofocus True
        , type' "text"
        , value model.newText
        , onInput NewTodoTextChange
        , onKeyUp NewTodoKeyUp
        ]
        []
    , (model.newText /= "") ? delButton <| div [] []
    , addButton
    ]


addButton : Html Msg
addButton =
  div [ class "todo-cmp__button" ]
    [ div
        [ class "todo-cmp__add-button"
        , onClick TodoAddNew
        ]
        []
    ]


delButton : Html Msg
delButton =
  div [ class "todo-cmp__button" ]
    [ div
        [ class "todo-cmp__del-button"
        , onClick NewTodoTodoClearText
        ]
        []
    ]
