require('./styles/main.scss');

var Elm = require('../elm/Main');

var appVersion = '0.0.0';

var storedState = localStorage.getItem('model');
var modelVersion = localStorage.getItem('version');
var startingState = storedState ? JSON.parse(storedState) : null;

if (modelVersion !== appVersion) {
    localStorage.setItem('model', null);
    localStorage.setItem('version', appVersion);
    startingState = null;
}

if (startingState) startingState.url = document.location.hash.replace("#/", '');
var todomvc = Elm.Main.fullscreen(startingState);

todomvc.ports.setStorage.subscribe(function(state) {
    window.history.pushState(null, null, '/#/' + state.url);
    localStorage.setItem('model', JSON.stringify(state));


})

